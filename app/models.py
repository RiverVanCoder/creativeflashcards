from datetime import datetime
from flask_login import UserMixin
from app import login
import random
from werkzeug.security import generate_password_hash, check_password_hash
from app import db

class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    cards = db.relationship('Card', backref='author', lazy='dynamic')

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    @login.user_loader
    def load_user(id):
        return User.query.get(int(id))

    def randCard(self):
        rand = random.randrange(0, Card.query.filter_by(user_id=self.id).count())
        return Card.query.filter_by(user_id=self.id)[rand]

    def getCard(self, idWanted):
        return Card.query.filter_by(id=idWanted).first()

    def show_cards(self):
        return Card.query.filter_by(user_id=self.id).all()





class Card(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    keyword = db.Column(db.String(140))
    definition = db.Column(db.String(140), default='')
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __repr__(self):
        return '<Card {}>'.format(self.keyword)
