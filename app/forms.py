from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, TextAreaField
from wtforms.validators import DataRequired, ValidationError, Email, EqualTo, Length
from app.models import User, Card

class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')

class TestForm(FlaskForm):
    keyword = StringField('keyword', validators=[DataRequired()])
    definition = TextAreaField('definition', validators=[DataRequired(),Length(0,140)])
    submit = SubmitField('Submit')

class EditCardForm(FlaskForm):
    keyword = StringField('keyword', validators=[DataRequired()])
    definition = TextAreaField('definition', validators=[DataRequired()])
    submit = SubmitField('Submit')

class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField('Repeat Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Register')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user is not None:
            raise ValidationError('Please use a different username. That is taken.')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('Please use a different email that one is taken')

class CreateCardForm(FlaskForm):
    keyword = StringField('Keyword: ', validators=[DataRequired()])
    definition = TextAreaField('Definition: ', validators=[DataRequired()])
    submit = SubmitField('Submit')
